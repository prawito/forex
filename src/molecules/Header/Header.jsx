// library
import React from 'react';

// style & assets
import './Header.scss';

const Header = () => {
    return (
        <div className="header">
            <div className="logo">
                <p>Forex App</p>
            </div>
        </div>
    )
}

export default React.memo(Header);